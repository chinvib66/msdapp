import React, { Fragment } from "react";
import { mapping, light as lightTheme } from "@eva-design/eva";
import { Text } from "react-native";
import { ApplicationProvider, IconRegistry } from "react-native-ui-kitten";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import { Router } from "./nav/Routes";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import configureStore from "./store";
import { LoadingScreen } from "./screens/helpers";
const { store, persistor } = configureStore();
import { MaterialCommunityIconsPack } from "./assets/icons/material-community";

export default class App extends React.Component {
	render() {
		return (
			<>
				<IconRegistry icons={MaterialCommunityIconsPack} />
				<ApplicationProvider theme={lightTheme} mapping={mapping}>
					<Provider store={store}>
						<PersistGate
							loading={<LoadingScreen />}
							persistor={persistor}>
							<Router />
						</PersistGate>
					</Provider>
				</ApplicationProvider>
			</>
		);
	}
}
