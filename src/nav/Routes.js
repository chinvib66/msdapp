import React, { Component } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { enableScreens } from "react-native-screens";
import { connect } from "react-redux";

import {
	CheckAuthScreen,
	LoadingScreen,
	QRScanScreen,
} from "../screens/helpers";
import {
	LoginScreen,
	LogoutScreen,
	ProfileScreen,
	RegisterScreen,
} from "../screens/auth";
import { Homescreen } from "../screens/HomeScreen";
import {
	InstallerTaskDetailScreen,
	InstallerDashboardScreen,
	InstallerCreateTaskScreen,
	IDUComplaintDetailScreen,
	IDUComplaintsScreen,
} from "../screens/installer";
import {
	SurveyorTaskDetailScreen,
	SurveyorDashboardScreen,
	SurveyorCreateTaskScreen,
} from "../screens/surveyor";
import TestScreen from "../screens/helpers/TestScreen";
import CameraScreen from "../screens/installer/Camera";
import {
	DUserDashboardScreen,
	DUserCreateTaskScreen,
	DUserTaskDetailScreen,
} from "../screens/duser";
import { NgoDashboardScreen, AddWorkerScreen } from "../screens/ngo";
//Screens

// const DefaultNavigator = {
// 	["Login"]: LoginScreen,
// };

// Only available on SignIn
// const ProtectedNavigator = {
// 	...InstallerNavigator,
// 	...SurveyorNavigator,
// };
const CommonNavigator = {
	["Profile"]: ProfileScreen,
	["Camera"]: CameraScreen,
	["QRScan"]: QRScanScreen,
};

const InstallerNavigator = {
	["IDashboard"]: InstallerDashboardScreen,
	["ITaskDetail"]: InstallerTaskDetailScreen,
	["ICreateTask"]: InstallerCreateTaskScreen,
	["IDUComplaints"]: IDUComplaintsScreen,
	["IDUComplaintDetail"]: IDUComplaintDetailScreen,
};

const SurveyorNavigator = {
	["SDashboard"]: SurveyorDashboardScreen,
	["STaskDetail"]: SurveyorTaskDetailScreen,
	["SCreateTask"]: SurveyorCreateTaskScreen,
};

const DUserNavigator = {
	["UDashboard"]: DUserDashboardScreen,
	["UTaskDetail"]: DUserTaskDetailScreen,
	["UCreateTask"]: DUserCreateTaskScreen,
};

const NGONavigator = {
	["NDashboard"]: NgoDashboardScreen,
	["AddWorker"]: AddWorkerScreen,
};

const InitialRouteName = "Check Auth";

const DefaultNavigator = createStackNavigator(
	{
		["Login"]: LoginScreen,
		["Register"]: RegisterScreen,
		["Loading"]: LoadingScreen,
		["Test"]: TestScreen,
		// ...InstallerNavigator,
		// ...SurveyorNavigator,
	},
	{
		initialRouteName: "Login",
		headerMode: "screen",
		defaultNavigationOptions: {
			header: null,
		},
	}
);

const ProtectedNavigator = createStackNavigator(
	{
		["Check Auth"]: CheckAuthScreen,
		["Logout"]: LogoutScreen,
		...CommonNavigator,
		...InstallerNavigator,
		...SurveyorNavigator,
		...DUserNavigator,
		...NGONavigator,
	},
	{
		initialRouteName: "Check Auth",
		headerMode: "screen",
		defaultNavigationOptions: {
			header: null,
		},
	}
);

const createAppRouter = container => {
	enableScreens();
	return createAppContainer(container);
};

// export const AppContainer = createAppRouter(AppNavigator);

class AppContainer extends Component {
	componentDidMount() {}
	render() {
		// enableScreens();
		if (this.props.auth.loggedIn) {
			const AppC = createAppRouter(ProtectedNavigator);
			return <AppC />;
		} else {
			const AppC = createAppRouter(DefaultNavigator);
			return <AppC />;
		}
	}
}
const mapStateToProps = state => ({
	auth: state.auth,
});

export const Router = connect(mapStateToProps)(AppContainer);
