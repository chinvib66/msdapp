import React, { Component } from "react";
import { View } from "react-native";
import { Input, Button, Text } from "react-native-ui-kitten";
import { connect } from "react-redux";

import { login } from "../../store/auth/actions";
import { INSTALLER, SURVEYOR, NGO } from "../../store";

class Login extends Component {
	state = {
		uid: null,
		password: null,
		message: null,
		loggingIn: false,
		buttonDisabled: false,
	};

	onChangeUid = uid => {
		if (!this.state.loggingIn) this.setState({ uid });
	};

	onChangePassword = password => {
		if (!this.state.loggingIn) this.setState({ password });
	};

	_logIn = () => {
		this.props
			.login(this.state.uid, this.state.password)
			.then(res => {
				if (res) {
					this.setState({ message: null, buttonDisabled: false });
					switch (this.props.auth.uType) {
						case INSTALLER:
							this.props.navigation.navigate("IDashboard");
							break;
						case SURVEYOR:
							this.props.navigation.navigate("SDashboard");
							break;
						case NGO:
							this.props.navigation.navigate("NDashboard");
						default:
							console.log("default");
							break;
					}
				} else {
					this.setState({ message: "Wrong Credentials" });
					setTimeout(() => {
						this.setState({ message: null });
					}, 6000);
				}
			})
			.catch(err =>
				this.setState({
					message: err,
					buttonDisabled: false,
				})
			)
			.catch(err =>
				this.setState({
					message: "Some Error Occured",
					buttonDisabled: false,
				})
			);
	};

	render() {
		return (
			<View style={this.props.style}>
				<Input
					size="small"
					placeholder="Unique Id"
					value={this.state.uid}
					onChangeText={this.onChangeUid}
					label="Unique Id"
					style={{ marginBottom: 10 }}
				/>
				<Input
					size="small"
					placeholder="Password"
					value={this.state.password}
					onChangeText={this.onChangePassword}
					label="Password"
				/>
				<Text style={{ textAlign: "center", marginTop: 20 }}>
					{this.state.message}
				</Text>
				<Button
					onPress={() => {
						this.setState({
							message: "Logging In...",
							buttonDisabled: true,
						});
						this._logIn();
					}}
					disabled={this.state.buttonDisabled}
					style={{ margin: 20 }}>
					Login
				</Button>
			</View>
		);
	}
}

const mapStateToProps = state => ({ auth: state.auth });
const mapDispatchToProps = dispatch => ({
	login: (username, password) => dispatch(login(username, password)),
});

export const LoginComponent = connect(
	mapStateToProps,
	mapDispatchToProps
)(Login);
