import React, { Component } from "react";
import { View } from "react-native";
import { Input, Button, Text } from "react-native-ui-kitten";
import { connect } from "react-redux";

import { login } from "../../store/auth/actions";
import { INSTALLER, SURVEYOR } from "../../store";

class Register extends Component {
	state = {
		data: { mobile: null, password: null },
		message: null,
		registering: false,
	};

	onChangeMobile = mobile => {
		if (!this.state.registering) this.setState({ data: { mobile } });
	};

	onChangePassword = password => {
		if (!this.state.registering) this.setState({ data: { password } });
	};

	_register = () => {
		this.props
			.register(this.state.data)
			.then(res => {
				if (res) {
					this.setState({ message: null });
					switch (this.props.auth.uType) {
						case INSTALLER:
							this.props.navigation.navigate("IDashboard");
							break;
						case SURVEYOR:
							this.props.navigation.navigate("SDashboard");
							break;
						default:
							console.log("default");
							break;
					}
				} else {
					this.setState({ message: "Wrong Credentials" });
					setTimeout(() => {
						this.setState({ message: null });
					}, 6000);
				}
			})
			.catch(err => this.setState({ message: err }));
	};

	render() {
		return (
			<View style={this.props.style}>
				<Input
					placeholder="Mobile"
					value={this.state.mobile}
					keyboardType={"phone-pad"}
					onChangeText={this.onChangeMobile}
					label="Mobile (without country code)"
				/>
				{this.state.data.mobile !== null ? (
					<Text style={{ marginBottom: 5 }}>
						Your Unique ID would be {"DU_" + this.state.data.mobile}
					</Text>
				) : null}
				<Input
					placeholder="Password"
					value={this.state.password}
					onChangeText={this.onChangePassword}
					label="Password"
				/>

				<Text style={{ textAlign: "center", marginTop: 20 }}>
					{this.state.message}
				</Text>
				<Button
					onPress={() => {
						this.setState({ message: "Registering..." });
						this._register();
					}}
					style={{ margin: 20 }}>
					Register
				</Button>
			</View>
		);
	}
}

const mapStateToProps = state => ({ auth: state.auth });
const mapDispatchToProps = dispatch => ({
	login: (username, password) => dispatch(login(username, password)),
});

export const RegisterComponent = connect(
	mapStateToProps,
	mapDispatchToProps
)(Register);
