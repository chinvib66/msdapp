import React, { Component } from "react";
import { View } from "react-native";
import { Text } from "react-native-ui-kitten";
import { connect } from "react-redux";
import { getUser } from "../../store/auth/actions";

class Profile extends Component {
	state = {
		edit: false,
	};
	componentDidMount() {
		this.props
			.getUser()
			// .then(res => this.setState(res))
			.then(console.log)
			.catch(err => console.log(err));
	}

	toggleEdit() {
		this.setState({ edit: !this.state.edit });
	}
	render() {
		return (
			<View>
				<View>
					<Text>Profile Page: {this.props.msduser.uType}</Text>
				</View>
				<View></View>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	msduser: { uType: state.auth.uType, data: null /*state.profile*/ },
});

const mapDispatchToProps = {
	getUser: getUser,
};

export const ProfileComponent = connect(
	mapStateToProps,
	mapDispatchToProps
)(Profile);
