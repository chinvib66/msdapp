import React, { Component } from "react";
import { View } from "react-native";

import { TASK_TYPES } from "./index";

const TASK_TYPE = TASK_TYPES.REPAIR;

class Repair extends Component {
	render() {
		return (
			<View>
				<Text>Repair</Text>
			</View>
		);
	}
}

export const IRepairComponent = Repair;
