import React, { Component } from "react";
import { View } from "react-native";

import { TASK_TYPES } from "./index";

const TASK_TYPE = TASK_TYPES.INSTALL;

class Install extends Component {
	render() {
		return (
			<View>
				<Text>Install</Text>
			</View>
		);
	}
}

export const IInstallComponent = Install;
