import React, { Component } from "react";
import {
	Button,
	StyleSheet,
	View,
	StatusBar,
	Dimensions,
	TouchableOpacity,
	Text,
	Image,
	PermissionsAndroid,
	ScrollView,
	Alert,
	BackHandler,
} from "react-native";
import { dirPicutures } from "./dirStorage";
import { RNCamera } from "react-native-camera";
import Svg, { Circle } from "react-native-svg";
import { Modal, Layout } from "react-native-ui-kitten";
import Grid from "react-native-grid-component";
const RNFS = require("react-native-fs");
const moment = require("moment");

let { height, width } = Dimensions.get("window");
let orientation = height > width ? "Portrait" : "Landscape";

async function requestPermissions() {
	try {
		const granted = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
			{
				title: "MSD App Read Permission",
				message: "MSD App needs access to your Storage ",
				buttonNeutral: "Ask Me Later",
				buttonNegative: "Cancel",
				buttonPositive: "OK",
			}
		);
		const granted1 = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.CAMERA,
			{
				title: "MSD App Camera Permission",
				message: "MSD App needs access to your Camera ",
				buttonNeutral: "Ask Me Later",
				buttonNegative: "Cancel",
				buttonPositive: "OK",
			}
		);
		const granted2 = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
			{
				title: "MSD App Write Permission",
				message: "MSD App needs access to your Storage ",
				buttonNeutral: "Ask Me Later",
				buttonNegative: "Cancel",
				buttonPositive: "OK",
			}
		);
		if (granted2 === PermissionsAndroid.RESULTS.GRANTED) {
			console.log("You can use the storage");
			return true;
		} else {
			console.log("Storage permission denied");
			return false;
		}
	} catch (err) {
		console.warn(err);
	}
}

const moveAttachment = async (filePath, newFilepath) => {
	return new Promise((resolve, reject) => {
		RNFS.mkdir(dirPicutures)
			.then(() => {
				RNFS.moveFile(filePath, newFilepath)
					.then(() => {
						console.log("FILE MOVED", filePath, newFilepath);
						resolve(true);
					})
					.catch(error => {
						console.log("moveFile error", error);
						reject(error);
					});
			})
			.catch(err => {
				console.log("mkdir error", err);
				reject(err);
			});
	});
};

const PendingView = () => (
	<View
		style={{
			flex: 1,
			backgroundColor: "white",
			justifyContent: "center",
			alignItems: "center",
			width: "100%",
		}}>
		<Text>Waiting</Text>
	</View>
);

class ImageSelectorModal extends Component {
	state = {
		selected: [...this.props.selected],
	};

	/// #TODO# USe RNFS Blob to display images from folder instead of just current images

	_renderItem = (data, i) => {
		return (
			<TouchableOpacity
				onPress={() => {
					var { selected } = this.state;
					if (selected.some(photo => photo.uri === data.uri)) {
						this.setState(state => {
							var newSelected = selected.filter(ele => {
								return ele.uri !== data.uri;
							});
							return {
								...state,
								selected: newSelected,
							};
						});
					} else {
						this.setState({
							...this.state,
							selected: [...this.state.selected, data],
						});
					}
				}}
				style={{
					minWidth: 60,
					margin: 3,
					flex: 1,
					borderWidth: 3,
					borderColor: this.state.selected.some(
						photo => photo.uri === data.uri
					)
						? "blue"
						: "white",
				}}>
				<Image
					source={{ uri: "file://" + data.uri }}
					style={{
						height: 120,
					}}
				/>
			</TouchableOpacity>
		);
	};
	toggleSelect = index => {};
	render() {
		return (
			<Modal
				allowBackdrop={true}
				backdropStyle={styles.backdrop}
				onBackdropPress={this.props.toggleModal}
				visible={this.props.visible}
				style={{}}>
				<View style={styles.modalContainer}>
					<View
						style={{
							flexDirection: "row",
							borderBottomWidth: 0.4,
						}}>
						<Text style={{ flex: 8 }}>
							Select atleast {this.props.count} photos.
						</Text>
						<Text style={{ flex: 4, textAlign: "right" }}>
							{this.state.selected.length}
						</Text>
					</View>
					<ScrollView style={{ paddingTop: 6 }}>
						<Grid
							style={styles.list}
							renderItem={this._renderItem}
							renderPlaceholder={this._renderPlaceholder}
							data={this.props.images}
							numColumns={3}
						/>
					</ScrollView>
					<View>
						<Button
							disabled={
								this.state.selected.length < this.props.count
							}
							onPress={() => {
								Alert.alert(
									"Confirm Selection",
									"Make sure to remove any blur photo",
									[
										{
											text: "Cancel",
											onPress: () =>
												console.log("Cancel Pressed"),
											style: "cancel",
										},
										{
											text: "OK",
											onPress: () => {
												console.log("OK Pressed");
												// console.log(
												// 	this.state.selected
												// );
												this.props.returnImages([
													...this.state.selected,
												]);
												this.props.goBack();
											},
										},
									],
									{ cancelable: true }
								);
							}}
							title="Select"></Button>
					</View>
				</View>
			</Modal>
		);
	}
}

class CameraComponent extends Component {
	state = {
		orientation: orientation,
		photo: [...this.props.selected],
		visible: false,
		selected: [...this.props.selected],
	};

	componentDidMount() {
		const a = requestPermissions();
	}
	componentWillMount() {
		Dimensions.addEventListener("change", this.handleOrientationChange);
		// BackHandler.addEventListener("hardwareBackPress", () =>
		// 	this.handleBack()
		// );
	}

	componentWillUnmount() {
		Dimensions.removeEventListener("change", this.handleOrientationChange);
		// BackHandler.removeEventListener("hardwareBackPress", () =>
		// 	this.handleBack()
		// );
	}

	// handleBack = () => {
	// 	if (this.state.selected.length <= this.props.count) {
	// 		Alert.alert(
	// 			"Less Photos Selected",
	// 			"You havent selected required amount of photos. Are you sure you want to go back?",
	// 			[
	// 				{
	// 					text: "Cancel",
	// 					onPress: () => console.log("Cancel Pressed"),
	// 					style: "cancel",
	// 				},
	// 				{
	// 					text: "OK",
	// 					onPress: () => this.props.navigation.goBack(null),
	// 				},
	// 			]
	// 		);
	// 	}
	// };

	handleOrientationChange = dimensions => {
		({ height, width } = dimensions.window);
		orientation = height > width ? "Portrait" : "Landscape";
		this.setState({ orientation });
	};

	saveImage = async file => {
		try {
			// set new image name and filepath
			const newImageName = `${moment().format("DDMMYY_HHmmSSS")}.jpg`;
			const newFilepath = `${dirPicutures}/${newImageName}`;
			// move and save image to new filepath
			const imageMoved = await moveAttachment(file.uri, newFilepath);
			if (imageMoved)
				this.setState({
					...this.state,
					photo: [
						...this.state.photo,
						{
							uri: newFilepath,
							filename: newImageName,
							type: "image/jpeg",
						},
					],
				});

			console.log("image moved", imageMoved);
		} catch (error) {
			console.log(error);
		}
	};

	toggleModal = () => {
		this.setState({ visible: !this.state.visible });
	};

	takePicture(camera) {
		const options = { quality: 0.84 };
		camera
			.takePictureAsync(options)
			.then(data => {
				this.saveImage(data);
			})
			.catch(err => {
				console.error("capture picture error", err);
			});
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<StatusBar barStyle="light-content" translucent />

				<RNCamera
					type={RNCamera.Constants.Type.back}
					captureAudio={false}
					style={styles.preview}
					orientation="auto">
					{({ camera, status }) => {
						if (status !== "READY") return <PendingView />;
						return (
							<>
								<View
									style={[
										StyleSheet.absoluteFill,
										{
											alignItems: "center",
											justifyContent: "center",
										},
									]}>
									<ImageSelectorModal
										toggleModal={this.toggleModal}
										visible={this.state.visible}
										images={this.state.photo}
										count={this.props.count}
										images={this.state.photo}
										selected={this.state.selected}
										returnImages={this.props.returnImages}
										goBack={() => {
											this.toggleModal();
											this.props.goBack();
										}}
									/>
									<Svg
										height="100%"
										width="100%"
										viewBox="0 0 100 100">
										<Circle
											cx="50"
											cy="50"
											r={this.props.r}
											stroke="white"
											strokeWidth="0.6"
											fill="transparent"
										/>
									</Svg>
								</View>
								<View
									style={[
										this.state.orientation === "Portrait"
											? styles.buttonContainerPortrait
											: styles.buttonContainerLandscape,
									]}>
									<TouchableOpacity
										style={{
											flex: 1,
											alignItems: "center",
											justifyContent: "center",
										}}
										onPress={this.toggleModal}>
										{this.state.photo.length !== 0 && (
											<Image
												source={{
													uri:
														"file://" +
														this.state.photo[
															this.state.photo
																.length - 1
														].uri,
												}}
												style={{
													width: 60,
													height: 60,
													borderWidth: 1,
													borderColor: "white",
												}}
											/>
										)}
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.takePicture(camera)}
										style={[
											this.state.orientation ===
											"Portrait"
												? styles.buttonPortrait
												: styles.buttonLandscape,
											,
											{
												flex: 1,
												alignItems: "center",
											},
										]}>
										<Image
											style={{ height: 60, width: 60 }}
											source={require("../../assets/images/camera-icon.png")}
										/>
									</TouchableOpacity>
									<View
										style={{
											flex: 1,
											alignItems: "center",
											justifyContent: "center",
										}}>
										<Text style={{ color: "white" }}>
											Count: {this.state.photo.length}
										</Text>
										<Text style={{ color: "white" }}>
											Remaining:{" "}
											{this.state.photo.length <
											this.props.count
												? this.props.count -
												  this.state.photo.length
												: 0}
										</Text>
									</View>
								</View>
							</>
						);
					}}
				</RNCamera>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	buttonContainerPortrait: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		flexDirection: "row",
		// justifyContent: "center",
		backgroundColor: "rgba(0, 0, 0, 0.9)",
		color: "white",
	},
	buttonContainerLandscape: {
		position: "absolute",
		bottom: 0,
		top: 0,
		right: 0,
		flexDirection: "column",
		justifyContent: "center",
		backgroundColor: "rgba(0, 0, 0, 0.5)",
		color: "white",
	},
	buttonPortrait: {
		backgroundColor: "transparent",
		padding: 5,
		marginHorizontal: 20,
	},
	buttonLandscape: {
		backgroundColor: "transparent",
		padding: 5,
		marginVertical: 20,
	},
	preview: {
		flex: 1,
		justifyContent: "flex-end",
		alignItems: "center",
	},
	backdrop: {
		backgroundColor: "rgba(0, 0, 0, 0.5)",
	},
	modalContainer: {
		backgroundColor: "white",
		height: 400,
		width: 300,
		borderRadius: 6,
		paddingVertical: 10,
		paddingHorizontal: 15,
	},
});

export default CameraComponent;
