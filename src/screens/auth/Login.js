import React, { Component } from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Layout, Button, Text } from "react-native-ui-kitten";
import { connect } from "react-redux";
import { LoginComponent } from "../../components/auth";
import { ScrollableAvoidKeyboard } from "../../components/helpers/scrollableAvoid";

class Login extends Component {
	render() {
		return (
			<ScrollableAvoidKeyboard style={{ flex: 1 }}>
				<Layout style={styles.layout}>
					<View style={styles.headerView}>
						<Text style={styles.headerText}>Login</Text>
					</View>
					<View style={styles.formView}>
						<View
							style={{
								flex: 1,
								justifyContent: "center",
								alignItems: "center",
							}}>
							<Image
								source={require("../../assets/images/logo.png")}
							/>
						</View>
						<LoginComponent
							style={{ flex: 1 }}
							navigation={this.props.navigation}
						/>
						<View
							style={{
								flex: 1,
								justifyContent: "center",
								alignItems: "center",
							}}>
							<TouchableOpacity
								onPress={() =>
									this.props.navigation.navigate("Register")
								}>
								<Text>Register new Dome User</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Layout>
			</ScrollableAvoidKeyboard>
		);
	}
}

export const LoginScreen = Login;

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		flexDirection: "row",
		flexGrow: 1,
	},
	headerText: {
		fontSize: 30,
		flex: 2,
		paddingTop: 60,
		textAlign: "center",
	},
	formView: {
		flex: 11,
		paddingTop: 20,
	},
});
