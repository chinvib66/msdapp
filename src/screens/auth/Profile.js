import React, { Component } from "react";
import { View } from "react-native";
import { Layout } from "react-native-ui-kitten";
import { ProfileComponent } from "../../components/auth";

class Profile extends Component {
	render() {
		return (
			<Layout>
				<View>
					<ProfileComponent />
				</View>
			</Layout>
		);
	}
}
export const ProfileScreen = Profile;
