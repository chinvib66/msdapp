import React, { Component } from "react";
import { connect } from "react-redux";
import { LoadingScreen } from "../helpers";
import { logout } from "../../store/auth/actions";
class Logout extends Component {
	componentDidMount() {
		this.props.logout().then(() => {
			this.props.navigation.navigate("Login");
		});
	}
	render() {
		return <LoadingScreen />;
	}
}

mapStateToProps = () => ({});
mapDispatchToProps = { logout };

export const LogoutScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(Logout);
