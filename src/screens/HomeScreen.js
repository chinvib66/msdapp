import React, { Component } from "react";
import { View, Text } from "react-native";
import { Layout, Button } from "react-native-ui-kitten";
import { connect } from "react-redux";
import { logout } from "../store/auth/actions";

class Home extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    }

    componentDidMount() {
        console.log("Home Mounted");
    }

    logout() {
        this.props
            .logout()
            .then(res => this.props.navigation.navigate("Check Auth"));
    }

    render() {
        return (
            <Layout style={{ flex: 1 }}>
                <Text>Home Screen</Text>
                <Button onPress={() => this.logout()}>Logout</Button>
            </Layout>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logout()),
});
export const Homescreen = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
