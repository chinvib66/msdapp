import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Layout, Button, Input } from "react-native-ui-kitten";
import { ScrollableAvoidKeyboard } from "../../components/helpers/scrollableAvoid";
import { INSTALLER, SURVEYOR } from "../../store";
import axios from "axios";
import { apiHost } from "../../config";

class Form extends Component {
	state = {
		data: {
			utype: this.props.navigation.state.params.type,
		},
		message: null,
		submitting: false,
	};

	onChangeField = (name, value) => {
		this.setState({
			...this.state,
			data: {
				...this.state.data,
				[name]: value,
			},
		});
	};

	onSubmit = () => {
		if (!this.state.data.mobile || this.state.data.mobile.length !== 10)
			this.setState({ message: "Please fill 10 digit mobile" });
		else if (!this.state.data.password)
			this.setState({ message: "Please fill password" });
		else if (!this.state.data.ngo)
			this.setState({ message: "Please fill NGO Name" });
		else if (!this.state.data.name)
			this.setState({ message: "Please fill Name" });
		else this.setState({ submitting: true });

		if (!this.state.message)
			axios
				.post(apiHost + "/auth/register/", { ...this.state.data })
				.then(res => {
					console.log(res.data);
					this.setState({
						data: {
							utype: this.props.navigation.state.params.type,
						},
						message: `Registered  ${res.data.uid} Successfully`,
						submitting: false,
					});
				})
				.catch(err => console.log(err.response.data));
		console.log(this.state.data);
	};

	fields = [
		{
			name: "mobile",
			placeholder: "Mobile without country code",
			keyboardType: "phone-pad",
			label: "Mobile",
			required: true,
			style: { marginBottom: 10 },
		},
		{
			name: "password",
			placeholder: "Password",
			label: "Password",
			required: true,
			secureTextEntry: true,
		},
		{ name: "ngo", placeholder: "NGO", label: "NGO", required: true },
		{ name: "name", placeholder: "Name", label: "Name" },
		{ name: "address", placeholder: "Address", label: "Address" },
		{ name: "village", placeholder: "Village", label: "Village" },
		{ name: "taluka", placeholder: "Taluka", label: "Taluka" },
		{ name: "district", placeholder: "District", label: "District" },
		{ name: "state", placeholder: "State", label: "State" },
		{
			name: "pincode",
			placeholder: "Pincode",
			label: "Pincode",
			keyboardType: "numeric",
		},
	];

	render() {
		const { navigation } = this.props;
		let u = "";
		switch (navigation.state.params.type) {
			case INSTALLER:
				u = "Installer";
				break;
			case SURVEYOR:
				u = "Surveyor";
				break;
			default:
				return (
					<Layout style={styles.layout}>
						<Text>
							Invalid Request: {navigation.state.params.type}
						</Text>
					</Layout>
				);
		}

		return (
			<ScrollableAvoidKeyboard style={{ flex: 1 }}>
				<Layout style={styles.layout}>
					<View style={styles.headerView}>
						<Text style={styles.headerText}>Register {u}</Text>
					</View>
					<View style={styles.formView}>
						<View>
							{this.fields.map(ele => {
								return (
									<Input
										caption={
											ele.name === "mobile" &&
											this.state.data.mobile
												? `UniqueID is ${this.state.data.utype}_${this.state.data.mobile} `
												: null
										}
										{...ele}
										value={this.state.data[ele.name]}
										onChangeText={e =>
											this.onChangeField(ele.name, e)
										}
									/>
								);
							})}
						</View>
						<View>
							<Text
								style={{ alignSelf: "center", marginTop: 20 }}>
								{this.state.message}
								{this.state.submitting ? "Submitting" : null}
							</Text>
							<Button
								style={{
									marginTop: 10,
									width: "60%",
									alignSelf: "center",
								}}
								disabled={this.state.submitting}
								onPress={() => this.onSubmit()}>
								Submit
							</Button>
						</View>
					</View>
				</Layout>
			</ScrollableAvoidKeyboard>
		);
	}
}

export const AddWorkerScreen = Form;

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		flexDirection: "row",
		flexGrow: 1,
	},
	headerText: {
		fontSize: 30,
		flex: 2,
		// paddingTop: 60,
		textAlign: "center",
	},
	formView: {
		flex: 11,
		paddingTop: 20,
	},
});
