import React, { Component } from "react";
import {
	View,
	StyleSheet,
	TextInput,
	Alert,
	ToastAndroid,
	ScrollView,
	Image,
	TouchableOpacity,
} from "react-native";
import {
	Layout,
	Text,
	Input,
	Datepicker,
	CheckBox,
	Button,
	Icon,
} from "react-native-ui-kitten";
import { connect } from "react-redux";
import { COMPLAINT } from "../../store/duser/actions";
import { ScrollableAvoidKeyboard } from "../../components/helpers/scrollableAvoid";
import Grid from "react-native-grid-component";
import { apiHost, imageHost } from "../../config";

class TaskC extends Component {
	state = {
		loaded: false,
		schema: null,
		task: this.props.navigation.state.params.task,
		submitting: false,
	};

	componentDidMount() {
		const type = this.props.navigation.state.params.task.type;
		switch (type) {
			case COMPLAINT:
				// this.props.getSchema().then(() =>
				this.setState({
					schema: this.props.helpers.schema.duser.complaint,
					loaded: true,
					type: type,
				});
				// );

				break;
			// case FEEDBACK:
			// 	this.setState({
			// 		schema: this.props.helpers.schema.duser.feedback,
			// 		loaded: true,
			// 		type: type,
			// 	});
			// 	break;
			default:
				break;
		}
	}

	submitTask = () => {
		this.setState({ submitting: true });
		this.props.updateTask({ ...this.state.task }).then(res => {
			this.setState({ submitting: false });
			console.log("Done");
		});
	};

	setPID = pid => {
		this.setState({
			...this.state,
			task: {
				...this.state.task,
				pid: pid,
			},
		});
	};

	validFields = [
		"char",
		"date",
		"boolean",
		"integer",
		"primarykeyrelated",
		"serializermethod",
	];

	onFieldChange = (name, value) => {
		let a = value;
		if (name == "status") {
			Alert.alert(
				"Confirm Task Completion?",
				"Are you sure that you want to mark this task as completed?\n\nThis will submit whatever data filled",
				[
					{
						text: "OK",
						onPress: () => {
							this.setState({
								task: { ...this.state.task, status: true },
								submitting: true,
							}),
								this.props.updateTask({
									...this.state.task,
									status: true,
								});
						},
					},
					{
						text: "Cancel",
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel",
					},
				],
				{ cancelable: true }
			);
		} else this.setState({ task: { ...this.state.task, [name]: a } });
	};

	getDateObject = str => {
		return new Date(str);
	};

	getDateString = date => {
		return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
	};

	renderDateStringIn = date => {
		let dat = new Date(date);
		return `${dat.getDate()}-${dat.getMonth() + 1}-${dat.getFullYear()}`;
	};

	inputFields = {
		char: props => {
			if (props.name === "pid")
				return (
					<View style={{ flexDirection: "row" }}>
						<Input
							size="small"
							value={this.state.task.pid}
							disabled
							style={{ flex: 6, height: 45 }}
							{...props}
						/>
						<TouchableOpacity
							style={{
								flex: 4,
								height: 45,
								// aspectRatio: 1,
								padding: 0,
								alignItems: "center",
								justifyContent: "center",
							}}
							onPress={() => {
								this.props.navigation.navigate({
									routeName: "QRScan",
									params: {
										setPID: this.setPID.bind(this),
									},
								});
							}}
							textStyle={{ padding: 0, margin: 0 }}>
							<Icon
								style={{
									height: 30,
									aspectRatio: 1,
									padding: 0,
									margin: 0,
								}}
								tintColor={"blue"}
								name="qrcode-scan"
							/>
						</TouchableOpacity>
					</View>
				);
			return (
				<Input
					size="small"
					style={{ height: 45 }}
					onChangeText={text => this.onFieldChange(props.name, text)}
					{...props}
				/>
			);
		},
		date: props => {
			return (
				<Datepicker
					onSelect={date => this.onFieldChange(props.name, date)}
					date={this.getDateObject(props.value)}
					{...props}
				/>
			);
		},
		boolean: props => {
			return (
				<CheckBox
					onChange={checked =>
						this.onFieldChange(props.name, checked)
					}
					{...props}
				/>
			);
		},
		primarykeyrelated: props => {
			// return <Text>{this.props.uid}</Text>;
		},
		serializermethod: props => {
			// console.log(props.value);
			if (props.value.length !== 0)
				return (
					<ScrollView nestedScrollEnabled style={{ maxHeight: 360 }}>
						<Grid
							renderItem={this._renderItem}
							data={props.value}
							numColumns={3}
						/>
					</ScrollView>
				);
			else return <Text>No Images</Text>;
		},
	};

	_renderItem = (data, i) => (
		<TouchableOpacity
			style={{
				minWidth: 60,
				margin: 3,
				flex: 1,
			}}
			onPress={() => {
				// this.previewImage(data.image);
			}}>
			<Image
				source={{ uri: imageHost + data.image }}
				style={{
					height: 120,
				}}
			/>
		</TouchableOpacity>
	);

	bools = {
		status: { 0: "Pending", 1: "Completed" },
	};

	render() {
		var task = this.state.task;
		return (
			<ScrollableAvoidKeyboard style={{ flex: 1 }}>
				<Layout style={styles.layout}>
					<View style={styles.headerView}>
						<Text style={styles.headerText}>
							{task.type} Detail
						</Text>
					</View>
					<View style={styles.taskView}>
						{this.state.loaded ? (
							Object.keys(this.state.schema).map((ele, index) => {
								const field = this.state.schema[ele];
								// console.log
								return (
									<View
										key={index}
										style={styles.elementView}>
										<View style={{ flexDirection: "row" }}>
											<Text
												style={
													styles.infoText
												}>{`${ele}: `}</Text>
											<Text>
												{task[ele] !== null
													? field.type ===
													  "serializermethod"
														? null
														: field.type === "date"
														? `${this.renderDateStringIn(
																task[ele]
														  )}`
														: field.type ===
														  "boolean"
														? `${
																this.bools[ele][
																	Number(
																		task[
																			ele
																		]
																	)
																]
														  }`
														: field.type ==
														  "primarykeyrelated"
														? this.props.uid
														: `${task[ele]}`
													: null}
											</Text>
											{field.type === "serializermethod"
												? this.inputFields[
														"serializermethod"
												  ]({
														value: task[ele],
														name: ele,
												  })
												: null}
										</View>
									</View>
								);
							})
						) : (
							<Text>Loading...</Text>
						)}
					</View>
				</Layout>
			</ScrollableAvoidKeyboard>
		);
	}
}
const mapStateToProps = state => ({
	duser: state.duser,
	helpers: state.helpers,
	uid: state.auth.uid,
});

const mapDispatchToProps = dispatch => ({});

export const DUserTaskDetailScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(TaskC);

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		flexDirection: "row",
		flexGrow: 1,
	},
	headerText: {
		fontSize: 30,
		flex: 2,
		paddingTop: 20,
	},
	taskView: {
		flex: 11,
		paddingTop: 20,
	},
	infoText: {
		textTransform: "capitalize",
		flexDirection: "row",
	},
	elementView: {
		// flexDirection: "row",
		// alignItems: "center",
		// minHeight: 50,
		marginVertical: 10,
	},
});
