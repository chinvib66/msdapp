import React, { Component } from "react";
import { View, StyleSheet, BackHandler } from "react-native";
import {
	Layout,
	Text,
	List,
	ListItem,
	Button,
	Icon,
	OverflowMenu,
} from "react-native-ui-kitten";
import { connect } from "react-redux";
import { COMPLAINT } from "../../store/duser/actions";
import {
	INSTALL,
	REPAIR,
	loadComplaints,
	// loadInstallations,
	// loadRepairs,
} from "../../store/installer/actions";

class Complaints extends Component {
	state = {
		addVisible: false,
		menuVisible: false,
		loaded: false,
	};

	componentDidMount() {
		this.loadData().then(state => this.setState({ loaded: true }));
		// .then(() => console.log("state", this.state));
	}

	loadData = () =>
		new Promise(resolve => {
			this.props.loadComplaints().then(status => console.log(status));
			// this.props.loadFeedbacks().then(status => console.log(status));
			resolve(true);
		});

	onClickAdd = () => {
		this.setState({ ...this.state, addVisible: !this.state.addVisible });
	};
	onClickMenu = () => {
		this.setState({ ...this.state, menuVisible: !this.state.menuVisible });
	};

	renderItem = ({ item, index }) => (
		<ListItem
			style={styles.listItemStyle}
			description={`${item.pid}`}
			title={`${item.type}`}
			onPress={() =>
				this.props.navigation.navigate({
					routeName: "IDUComplaintDetail",
					params: { task: item },
				})
			}
		/>
	);

	mapMenuRouteAndType = [
		{ routeName: "IDashboard" },
		{ routeName: "Profile" },
		{ routeName: "Logout" },
	];

	mapAddRouteAndType = [
		{ routeName: "ICreateTask", params: { type: REPAIR } },
		{ routeName: "ICreateTask", params: { type: INSTALL } },
	];

	render() {
		var tasks = [];
		var complaints = [];
		// var feedbacks = [];
		if (this.state.loaded == true) {
			complaints = this.props.installer.complaints;
			if (complaints !== null)
				complaints.forEach((elem, index) => {
					tasks.push({ type: COMPLAINT, ...elem });
				});
			// var itasks = [];
			// feedbacks = this.props.duser.feedbacks;
			// if (feedbacks !== null)
			// 	feedbacks.forEach((elem, index) => {
			// 		tasks.push({ type: FEEDBACK, ...elem });
			// 	});
		}

		return (
			<Layout style={styles.layout}>
				<View
					style={[
						styles.headerView,
						{ justifyContent: "space-between" },
					]}>
					<Text style={styles.headerText}>Dashboard</Text>
					<OverflowMenu
						onSelect={selected => {
							this.onClickMenu();
							this.props.navigation.navigate(
								this.mapMenuRouteAndType[selected]
							);
						}}
						data={[
							{ title: "Dashboard" },
							{ title: "Profile" },
							{ title: "Logout" },
						]}
						onBackdropPress={this.onClickMenu}
						visible={this.state.menuVisible}
						contentContainerStyle={styles.popUpListStyle}>
						<Button
							onPress={this.onClickMenu}
							style={styles.button}>
							Menu
						</Button>
					</OverflowMenu>
				</View>
				<View style={styles.upComingView}>
					<View>
						<Text style={styles.subHeadingText}>
							Unresolved Complaints
						</Text>
					</View>
					<View>
						<List
							nestedScrollEnabled={true}
							data={tasks.filter(elem => {
								return !elem.status;
							})}
							style={styles.listStyle}
							renderItem={this.renderItem}
						/>
					</View>
				</View>

				<View style={styles.popView}>
					<OverflowMenu
						onSelect={selected => {
							this.onClickAdd();
							this.props.navigation.navigate(
								this.mapAddRouteAndType[selected]
							);
						}}
						data={[
							{ title: "Add Repair" },
							{ title: "Add Installation" },
						]}
						placement={"right"}
						onBackdropPress={this.onClickAdd}
						visible={this.state.addVisible}
						contentContainerStyle={styles.popUpListStyle}>
						<Button
							style={styles.button}
							textStyle={styles.buttonText}
							onPress={this.onClickAdd}>
							+
						</Button>
					</OverflowMenu>
				</View>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({ installer: state.installer });
const mapDispatchToProps = dispatch => ({
	loadComplaints: () => dispatch(loadComplaints()),
	// loadFeedBacks: () => dispatch(loadFeedbacks()),
});

export const IDUComplaintsScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(Complaints);

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
		// flexDirection: "column",
		// justifyContent: "space-between",
	},
	listStyle: {
		// height: 360,
		borderRadius: 6,
		borderWidth: 0.5,
		marginBottom: 20,
		height: "90%",
	},
	listItemStyle: {
		borderWidth: 0.2,
		borderRadius: 6,
		margin: 7,
		// marginHorizontal: 5,
		padding: 5,
	},
	popUpListStyle: {
		borderWidth: 0.2,
		borderRadius: 6,
		marginVertical: 1,
		marginHorizontal: 1,
		padding: 5,
	},
	buttonText: {
		width: "100%",
		height: "100%",
		textAlign: "center",
		textAlignVertical: "center",
		// fontSize: 24,
		// marginTop: 12,
		// marginLeft: 10,
	},
	button: {
		// width: 45,
		// height: 45,
		borderRadius: 45,
		// fontSize: 40,
		paddingVertical: 6,
		// alignSelf: "flex-end",
		// alignContent: "center",
		justifyContent: "center",
		alignItems: "center",
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		// flexDirection: "row",
		// paddingVertical: 30,
		alignItems: "center",
		flexDirection: "row",
	},
	headerText: {
		fontSize: 30,
		// flex: 1,
		paddingTop: 20,
	},
	subHeadingText: {
		fontSize: 24,
		paddingVertical: 5,
		marginVertical: 5,
	},
	upComingView: {
		flex: 6.5,
		marginBottom: 20,
		paddingTop: 10,
	},
	finishedView: {
		flex: 4,
		marginBottom: 20,
		paddingTop: 10,
	},
	popView: {
		flex: 1,
		// position: "absolute",
		// right: 10,
		// bottom: 10,
		// flexDirection: "row",
		height: "100%",
		// width: 50,
		justifyContent: "flex-end",
		alignContent: "flex-end",
	},
});
