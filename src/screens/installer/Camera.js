import React, { Component } from "react";
import { View } from "react-native";
import CameraComponent from "../../components/camera/CameraComponent";

export default class CameraScreen extends Component {
	render() {
		return (
			<CameraComponent
				r={this.props.navigation.state.params.r}
				count={this.props.navigation.state.params.count}
				returnImages={this.props.navigation.state.params.returnImages}
				goBack={() => this.props.navigation.goBack()}
				selected={this.props.navigation.state.params.selected}
			/>
		);
	}
}
