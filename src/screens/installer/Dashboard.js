import React, { Component } from "react";
import {
	View,
	StyleSheet,
	BackHandler,
	ScrollView,
	RefreshControl,
	TouchableOpacity,
} from "react-native";
import {
	Layout,
	Text,
	List,
	ListItem,
	Button,
	Icon,
	OverflowMenu,
} from "react-native-ui-kitten";
import { connect } from "react-redux";
import {
	INSTALL,
	REPAIR,
	loadInstallations,
	loadRepairs,
} from "../../store/installer/actions";

class Dashboard extends Component {
	state = {
		addVisible: false,
		menuVisible: false,
		loaded: false,
	};

	componentDidMount() {
		this.loadData().then(state => this.setState({ loaded: true }));
		// .then(() => console.log("state", this.state));
	}

	loadData = () =>
		new Promise(resolve => {
			// if (this.props.installer.installations.length === 0) {
			this.props.loadInstallations().then(status => console.log(status));
			this.props.loadRepairs().then(status => console.log(status));
			resolve(true);
		});

	onClickAdd = () => {
		this.setState({ ...this.state, addVisible: !this.state.addVisible });
	};
	onClickMenu = () => {
		this.setState({ ...this.state, menuVisible: !this.state.menuVisible });
	};

	renderItem = ({ item, index }) => (
		<ListItem
			style={styles.listItemStyle}
			description={`${item.location}`}
			title={`${item.type}`}
			onPress={() =>
				this.props.navigation.navigate({
					routeName: "ITaskDetail",
					params: { task: item },
				})
			}
		/>
	);

	mapMenuRouteAndType = [
		{ routeName: "IDUComplaints" },
		// { routeName: "Profile" },
		{ routeName: "Logout" },
	];

	mapAddRouteAndType = [
		{ routeName: "ICreateTask", params: { type: REPAIR } },
		{ routeName: "ICreateTask", params: { type: INSTALL } },
	];

	render() {
		var tasks = [];
		var installations = [];
		var repairs = [];
		if (this.state.loaded == true) {
			installations = this.props.installer.installations;
			if (installations !== null)
				installations.forEach((elem, index) => {
					tasks.push({ type: INSTALL, ...elem });
				});
			// var itasks = [];
			repairs = this.props.installer.repairs;
			if (repairs !== null)
				repairs.forEach((elem, index) => {
					tasks.push({ type: REPAIR, ...elem });
				});
		}

		return (
			<Layout style={styles.layout}>
				<View
					style={[
						styles.headerView,
						{ justifyContent: "space-between" },
					]}>
					<Text style={styles.headerText}>Dashboard</Text>
					<TouchableOpacity onPress={() => this.componentDidMount()}>
						<Icon name="refresh" style={{ height: 30 }} />
					</TouchableOpacity>
					<OverflowMenu
						onSelect={selected => {
							this.onClickMenu();
							this.props.navigation.navigate(
								this.mapMenuRouteAndType[selected]
							);
						}}
						data={[
							{ title: "Complaints" },
							// { title: "Profile" },
							{ title: "Logout" },
						]}
						onBackdropPress={this.onClickMenu}
						visible={this.state.menuVisible}
						contentContainerStyle={styles.popUpListStyle}>
						<Button
							onPress={this.onClickMenu}
							style={styles.button}>
							Menu
						</Button>
					</OverflowMenu>
				</View>
				<View style={styles.upComingView}>
					<View style={{ flexDirection: "row" }}>
						<Text style={styles.subHeadingText}>
							Upcoming Tasks
						</Text>
					</View>
					<View>
						<List
							nestedScrollEnabled={true}
							data={tasks.filter(elem => {
								return !elem.status;
							})}
							style={styles.listStyle}
							renderItem={this.renderItem}
						/>
					</View>
				</View>
				<View style={styles.finishedView}>
					<View>
						<Text style={styles.subHeadingText}>
							Completed Tasks
						</Text>
					</View>
					<View>
						<List
							nestedScrollEnabled={true}
							data={tasks.filter(elem => {
								return elem.status;
							})}
							style={styles.listStyle}
							renderItem={this.renderItem}
						/>
					</View>
				</View>
				<View style={styles.popView}>
					<OverflowMenu
						onSelect={selected => {
							this.onClickAdd();
							this.props.navigation.navigate(
								this.mapAddRouteAndType[selected]
							);
						}}
						data={[
							{ title: "Add Repair" },
							{ title: "Add Installation" },
						]}
						placement={"right"}
						onBackdropPress={this.onClickAdd}
						visible={this.state.addVisible}
						contentContainerStyle={styles.popUpListStyle}>
						<Button
							style={styles.button}
							textStyle={styles.buttonText}
							onPress={this.onClickAdd}>
							+
						</Button>
					</OverflowMenu>
				</View>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({ installer: state.installer });
const mapDispatchToProps = dispatch => ({
	loadInstallations: () => dispatch(loadInstallations()),
	loadRepairs: () => dispatch(loadRepairs()),
});

export const InstallerDashboardScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(Dashboard);

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
		// flexDirection: "column",
		// justifyContent: "space-between",
	},
	listStyle: {
		// height: 360,
		borderRadius: 6,
		borderWidth: 0.5,
		marginBottom: 20,
		height: "90%",
	},
	listItemStyle: {
		borderWidth: 0.2,
		borderRadius: 6,
		margin: 7,
		// marginHorizontal: 5,
		padding: 5,
	},
	popUpListStyle: {
		borderWidth: 0.2,
		borderRadius: 6,
		marginVertical: 1,
		marginHorizontal: 1,
		padding: 5,
	},
	buttonText: {
		width: "100%",
		height: "100%",
		textAlign: "center",
		textAlignVertical: "center",
		// fontSize: 24,
		// marginTop: 12,
		// marginLeft: 10,
	},
	button: {
		// width: 45,
		// height: 45,
		borderRadius: 45,
		// fontSize: 40,
		paddingVertical: 6,
		// alignSelf: "flex-end",
		// alignContent: "center",
		justifyContent: "center",
		alignItems: "center",
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		// flexDirection: "row",
		// paddingVertical: 30,
		alignItems: "center",
		flexDirection: "row",
	},
	headerText: {
		fontSize: 30,
		// flex: 1,
		paddingTop: 20,
	},
	subHeadingText: {
		fontSize: 24,
		paddingVertical: 5,
		marginVertical: 5,
	},
	upComingView: {
		flex: 6.5,
		marginBottom: 20,
		paddingTop: 10,
	},
	finishedView: {
		flex: 4,
		marginBottom: 20,
		paddingTop: 10,
	},
	popView: {
		flex: 1,
		// position: "absolute",
		// right: 10,
		// bottom: 10,
		// flexDirection: "row",
		height: "100%",
		// width: 50,
		justifyContent: "flex-end",
		alignContent: "flex-end",
	},
});
