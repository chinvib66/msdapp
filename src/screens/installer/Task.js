import React, { Component } from "react";
import {
	View,
	StyleSheet,
	TextInput,
	Alert,
	ToastAndroid,
	ScrollView,
	Image,
	TouchableOpacity,
} from "react-native";
import {
	Layout,
	Text,
	Input,
	Datepicker,
	CheckBox,
	Button,
	Icon,
} from "react-native-ui-kitten";
import { connect } from "react-redux";
import { INSTALL, REPAIR, updateTask } from "../../store/installer/actions";
import { ScrollableAvoidKeyboard } from "../../components/helpers/scrollableAvoid";
import Grid from "react-native-grid-component";
import { apiHost, imageHost } from "../../config";

class TaskC extends Component {
	state = {
		loaded: false,
		schema: null,
		task: this.props.navigation.state.params.task,
		submitting: false,
	};

	componentDidMount() {
		const type = this.props.navigation.state.params.task.type;
		switch (type) {
			case INSTALL:
				// this.props.getSchema().then(() =>
				this.setState({
					schema: this.props.helpers.schema.installer.installation,
					loaded: true,
					type: type,
				});
				// );

				break;
			case REPAIR:
				this.setState({
					schema: this.props.helpers.schema.installer.repair,
					loaded: true,
					type: type,
				});
				break;
			default:
				break;
		}
	}

	submitTask = () => {
		this.setState({ submitting: true });
		this.props.updateTask({ ...this.state.task }).then(res => {
			this.setState({ submitting: false });
			console.log("Done");
		});
	};

	setPID = pid => {
		console.log(pid);
		this.setState({
			...this.state,
			task: {
				...this.state.task,
				pid: pid,
			},
		});
	};

	validFields = [
		"char",
		"date",
		"boolean",
		"integer",
		"primarykeyrelated",
		"serializermethod",
	];

	onFieldChange = (name, value) => {
		let a = value;
		if (name == "status") {
			Alert.alert(
				"Confirm Task Completion?",
				"Are you sure that you want to mark this task as completed?\n\nThis will submit whatever data filled",
				[
					{
						text: "OK",
						onPress: () => {
							this.setState({
								task: { ...this.state.task, status: true },
								submitting: true,
							}),
								this.props.updateTask({
									...this.state.task,
									status: true,
								});
						},
					},
					{
						text: "Cancel",
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel",
					},
				],
				{ cancelable: true }
			);
		} else this.setState({ task: { ...this.state.task, [name]: a } });
	};

	getDateObject = str => {
		return new Date(str);
	};

	getDateString = date => {
		return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
	};

	renderDateStringIn = date => {
		let dat = new Date(date);
		return `${dat.getDate()}-${dat.getMonth() + 1}-${dat.getFullYear()}`;
	};

	inputFields = {
		char: props => {
			if (props.name === "pid")
				return (
					<View style={{ flexDirection: "row" }}>
						<Input
							size="small"
							disabled
							value={this.state.task.pid}
							style={{ flex: 6, height: 45 }}
							{...props}
						/>
						<TouchableOpacity
							style={{
								flex: 4,
								height: 45,
								// aspectRatio: 1,
								padding: 0,
								alignItems: "center",
								justifyContent: "center",
							}}
							onPress={() => {
								this.props.navigation.navigate({
									routeName: "QRScan",
									params: {
										setPID: this.setPID.bind(this),
									},
								});
							}}
							textStyle={{ padding: 0, margin: 0 }}>
							<Icon
								style={{
									height: 30,
									aspectRatio: 1,
									padding: 0,
									margin: 0,
								}}
								tintColor={"blue"}
								name="qrcode-scan"
							/>
						</TouchableOpacity>
					</View>
				);
			return (
				<Input
					size="small"
					style={{ height: 45 }}
					onChangeText={text => this.onFieldChange(props.name, text)}
					{...props}
				/>
			);
		},
		date: props => {
			return (
				<Datepicker
					onSelect={date => this.onFieldChange(props.name, date)}
					date={this.getDateObject(props.value)}
					{...props}
				/>
			);
		},
		boolean: props => {
			return (
				<CheckBox
					onChange={checked =>
						this.onFieldChange(props.name, checked)
					}
					{...props}
				/>
			);
		},
		primarykeyrelated: props => {
			// return <Text>{this.props.uid}</Text>;
		},
		serializermethod: props => {
			// console.log(props.value);
			if (props.value !== null)
				if (props.name === "complaint_detail")
					return (
						<View style={{ marginLeft: 20, flex: 1 }}>
							{Object.keys(props.value).map(k => {
								let data = props.value[k];
								if (k === ("duser" || "status" || "id"))
									return null;
								if (k === "images")
									if (data !== null)
										if (data.length === 0) return null;
										else
											return (
												<>
													<Text>Images</Text>
													<ScrollView
														nestedScrollEnabled
														// horizontal={true}
														style={{
															maxHeight: 360,
														}}>
														<Grid
															renderItem={
																this._renderItem
															}
															data={data}
															numColumns={1}
															style={{
																flex: 1,
																padding: 10,
															}}
														/>
													</ScrollView>
												</>
											);
									else return null;
								return <Text>{`${k}: ${data}`}</Text>;
							})}
						</View>
					);
				else if (props.value.length !== 0)
					return (
						<ScrollView
							nestedScrollEnabled
							style={{ maxHeight: 360 }}>
							<Grid
								renderItem={this._renderItem}
								data={props.value}
								numColumns={1}
							/>
						</ScrollView>
					);
				else return <Text>No Images</Text>;
			else return <Text>None</Text>;
		},
	};

	_renderItem = (data, i) => (
		<TouchableOpacity
			style={{
				minWidth: 60,
				margin: 3,
				// flex: 1,
				// flexDirection: "row",
			}}
			key={i}
			onPress={() => {
				// this.previewImage(data.image);
			}}>
			<Image
				source={{ uri: imageHost + data.image }}
				style={{
					width: 120,
					aspectRatio: 1,
				}}
			/>
			{Object.keys(data).some(prop => prop === "isQualityGood") ? (
				<View style={{ flexDirection: "row" }}>
					<Text>IsQualityGood:</Text>
					<Text> {data.isQualityGood ? "Yes" : "No"}</Text>
				</View>
			) : null}
			{Object.keys(data).some(prop => prop === "allWorking") ? (
				<View style={{ flexDirection: "row" }}>
					<Text>All Leds Woking?:</Text>
					<Text> {data.allWorking ? "Yes" : "No"}</Text>
				</View>
			) : null}
			{Object.keys(data).some(prop => prop === "detectedLeds") ? (
				<View style={{ flexDirection: "row" }}>
					<Text>Number of Working Leds:</Text>
					<Text> {data.detectedLeds}</Text>
				</View>
			) : null}
		</TouchableOpacity>
	);

	bools = {
		status: { 0: "Pending", 1: "Completed" },
	};

	render() {
		var task = this.state.task;
		return (
			<ScrollableAvoidKeyboard style={{ flex: 1 }}>
				<Layout style={styles.layout}>
					<View style={styles.headerView}>
						<Text style={styles.headerText}>
							{task.type} Detail
						</Text>
					</View>
					<View style={styles.taskView}>
						{this.state.loaded ? (
							Object.keys(this.state.schema).map((ele, index) => {
								const field = this.state.schema[ele];
								// console.log
								return (
									<View
										key={index}
										style={styles.elementView}>
										<View style={{ flexDirection: "row" }}>
											<Text
												style={
													styles.infoText
												}>{`${ele}: `}</Text>
											<Text>
												{task[ele] !== null
													? field.type ===
													  "serializermethod"
														? null
														: field.type === "date"
														? `${this.renderDateStringIn(
																task[ele]
														  )}`
														: field.type ===
														  "boolean"
														? `${
																this.bools[ele][
																	Number(
																		task[
																			ele
																		]
																	)
																]
														  }`
														: field.type ==
														  "primarykeyrelated"
														? this.props.uid
														: `${task[ele]}`
													: null}
											</Text>
										</View>
										{!task.status
											? this.validFields.includes(
													field.type
											  )
												? field.editable
													? this.inputFields[
															field.type
													  ]({
															value: task[ele],
															name: ele,
													  })
													: field.type ===
													  "serializermethod"
													? this.inputFields[
															"serializermethod"
													  ]({
															value: task[ele],
															name: ele,
													  })
													: null
												: null
											: null}
									</View>
								);
							})
						) : (
							<Text>Loading...</Text>
						)}
						{task.status ? null : (
							<>
								<Text
									style={{ margin: 10, textAlign: "center" }}>
									{this.state.error | this.state.submitting
										? "Submitting"
										: null}
								</Text>
								<Button
									disabled={this.state.submitting}
									onPress={this.submitTask}>
									Update Details
								</Button>
							</>
						)}
					</View>
				</Layout>
			</ScrollableAvoidKeyboard>
		);
	}
}
const mapStateToProps = state => ({
	installer: state.installer,
	helpers: state.helpers,
	uid: state.auth.uid,
});

const mapDispatchToProps = dispatch => ({
	updateTask: task => dispatch(updateTask(task)),
});

export const InstallerTaskDetailScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(TaskC);

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		flexDirection: "row",
		flexGrow: 1,
	},
	headerText: {
		fontSize: 30,
		flex: 2,
		paddingTop: 20,
	},
	taskView: {
		flex: 11,
		paddingTop: 20,
	},
	infoText: {
		textTransform: "capitalize",
		flexDirection: "row",
	},
	elementView: {
		// flexDirection: "row",
		// alignItems: "center",
		// minHeight: 50,
		marginVertical: 10,
	},
});
