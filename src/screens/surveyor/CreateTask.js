import React, { Component } from "react";
import { View, Alert, StyleSheet, TouchableOpacity } from "react-native";
import {
	Layout,
	Text,
	Datepicker,
	Input,
	CheckBox,
	Button,
	Icon,
} from "react-native-ui-kitten";
import { connect } from "react-redux";
import { SURVEY, createTask } from "../../store/surveyor/actions";
import { ScrollableAvoidKeyboard } from "../../components/helpers/scrollableAvoid";

class CreateTask extends Component {
	state = {
		type: this.props.navigation.state.params.type,
		schema: null,
		task: {
			// installer: this.props.uid,
			type: this.props.navigation.state.params.type,
			status: false,
			// date: this.getDateString(Date.now()),
		},
		submitting: false,
	};

	componentDidMount() {
		switch (this.state.type) {
			case SURVEY:
				this.setState({ schema: this.props.schema.surveyor.survey });
				break;

			default:
				break;
		}
		this.setState({
			task: { ...this.state.task, date: this.getDateString(new Date()) },
		});
		// console.log(this.props.uid);
	}

	setPID = pid => {
		this.setState({
			...this.state,
			task: {
				...this.state.task,
				pid: pid,
			},
		});
	};

	validFields = ["char", "date", "boolean", "integer", "primarykeyrelated"];

	onFieldChange = (name, value) => {
		let a = value;
		if (name === "status" && value === true) {
			Alert.alert(
				"Confirm Task Completion?",
				"Are you sure that you want to mark this task as completed?",
				[
					{
						text: "OK",
						onPress: () => {
							this.setState({
								task: { ...this.state.task, status: true },
							});
							// this.props.updateTask({
							// 	...this.state.task,
							// 	status: true,
							// });
						},
					},
					{
						text: "Cancel",
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel",
					},
				],
				{ cancelable: true }
			);
		} else this.setState({ task: { ...this.state.task, [name]: a } });
	};

	getDateObject = str => {
		if (str === undefined || str === null) return new Date();
		return new Date(str);
	};

	getDateString = date => {
		return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
	};

	renderDateStringIn = date => {
		let dat = new Date(date);
		return `${dat.getDate()}-${dat.getMonth() + 1}-${dat.getFullYear()}`;
	};

	inputFields = {
		char: props => {
			if (props.name === "pid")
				return (
					<View style={{ flexDirection: "row" }}>
						<Input
							size="small"
							value={this.state.task.pid}
							disabled
							style={{ flex: 6, height: 45 }}
							{...props}
						/>
						<TouchableOpacity
							style={{
								flex: 4,
								height: 45,
								// aspectRatio: 1,
								padding: 0,
								alignItems: "center",
								justifyContent: "center",
							}}
							onPress={() => {
								this.props.navigation.navigate({
									routeName: "QRScan",
									params: {
										setPID: this.setPID.bind(this),
									},
								});
							}}
							textStyle={{ padding: 0, margin: 0 }}>
							<Icon
								style={{
									height: 30,
									aspectRatio: 1,
									padding: 0,
									margin: 0,
								}}
								tintColor={"blue"}
								name="qrcode-scan"
							/>
						</TouchableOpacity>
					</View>
				);
			return (
				<Input
					size="small"
					style={{ height: 45 }}
					onChangeText={text => this.onFieldChange(props.name, text)}
					{...props}
				/>
			);
		},
		date: props => {
			return (
				<Datepicker
					onSelect={date => this.onFieldChange(props.name, date)}
					date={this.getDateObject(props.value)}
					{...props}
				/>
			);
		},
		boolean: props => {
			return (
				<View style={{ flexDirection: "row" }}>
					<CheckBox
						onChange={checked =>
							this.onFieldChange(props.name, checked)
						}
						checked={props.value}
						{...props}
					/>
					<Text> {this.bools.status[props.value ? 1 : 0]}</Text>
				</View>
			);
		},
		primarykeyrelated: props => {
			return <Text>{this.props.uid}</Text>;
		},
	};

	bools = {
		status: { 0: "Pending", 1: "Completed" },
	};

	onSubmitPress = () => {
		this.setState({ submitting: true });
		this.props
			.createTask(this.state.task)
			.then(() => this.props.navigation.goBack())
			.catch(err => {
				this.setState({ error: err });
			});
	};

	render() {
		// console.log(this.state);
		return (
			<ScrollableAvoidKeyboard style={{ flex: 1 }}>
				<Layout style={styles.layout}>
					<View style={styles.headerView}>
						<Text style={styles.headerText}>
							Create {this.state.type}
						</Text>
					</View>
					<View style={styles.taskView}>
						{this.state.schema !== null
							? Object.keys(this.state.schema).map(
									(ele, index) => {
										// (ele.editable == true)?( return <Text>{ele}</Text>): return null;
										const field = this.state.schema[ele];
										if (field.editable === true)
											return (
												<View
													style={styles.elementView}
													key={index}>
													<Text
														style={{
															textTransform:
																"capitalize",
														}}>
														{ele}
													</Text>
													{this.validFields.includes(
														field.type
													)
														? field.editable
															? this.inputFields[
																	field.type
															  ]({
																	value: this
																		.state
																		.task[
																		ele
																	],
																	name: ele,
															  })
															: null
														: null}
												</View>
											);
									}
							  )
							: null}
						<Text style={{ margin: 10, textAlign: "center" }}>
							{this.state.error | this.state.submitting
								? "Submitting"
								: null}
						</Text>
						<Button
							disabled={this.state.submitting}
							onPress={this.onSubmitPress}>
							Submit
						</Button>
					</View>
				</Layout>
			</ScrollableAvoidKeyboard>
		);
	}
}

const mapStateToProps = state => ({
	schema: state.helpers.schema,
	uid: state.auth.uid,
});

const mapDispatchToProps = {
	createTask,
};

export const SurveyorCreateTaskScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(CreateTask);

const styles = StyleSheet.create({
	layout: {
		height: "100%",
		margin: 30,
		flex: 1,
	},
	headerView: {
		// minHeight: 70,
		// alignContent: "center",
		// alignItems: "center",
		flex: 1,
		flexDirection: "row",
		flexGrow: 1,
	},
	headerText: {
		fontSize: 30,
		flex: 2,
		paddingTop: 20,
	},
	taskView: {
		flex: 11,
		paddingTop: 20,
	},
	elementView: {
		margin: 5,
	},
});
