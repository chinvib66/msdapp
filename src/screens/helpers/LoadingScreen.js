import React, { Component } from "react";
import { View, Image } from "react-native";
import { Layout, Text } from "react-native-ui-kitten";

class Loading extends Component {
	componentWillUnmount() {
		// this.setState({ text: null });
	}
	state = { text: "..." };

	render() {
		return (
			<Layout
				style={{
					flex: 1,
					flexDirection: "row",
					// alignContent: "center",
					alignItems: "center",
					justifyContent: "center",
				}}>
				{this.props.children ? (
					this.props.children
				) : (
					<View
						style={{
							justifyContent: "center",
							alignItems: "center",
						}}>
						<Image
							source={require("../../assets/images/logo.png")}
						/>
						<Text>{this.state.text}</Text>
					</View>
				)}
			</Layout>
		);
	}
}

export const LoadingScreen = Loading;
