import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";

class ScanScreen extends Component {
	onSuccess = e => {
		console.log("scanned", e.data);
		this.props.navigation.state.params.setPID(e.data);
		this.props.navigation.goBack();
	};

	render() {
		return (
			<QRCodeScanner
				onRead={e => this.onSuccess(e)}
				showMarker={true}
				bottomContent={<Text>Scan the QR Code for PID</Text>}
			/>
		);
	}
}

export const QRScanScreen = ScanScreen;
