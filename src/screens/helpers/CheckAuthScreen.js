import React, { Component } from "react";
import { connect } from "react-redux";
import { checkLogin } from "../../store/auth/actions";
import { LoadingScreen } from "./";
import { INSTALLER, SURVEYOR, DUSER, NGO } from "../../store";
import { getSchema } from "../../store/helpers/actions";

class CheckAuth extends Component {
	_checkLogin() {
		this.props
			.checkLogin()
			.then(status => {
				this.props
					.getSchema()
					.then(status => {
						if (status) console.log("Schema fetched");
						else console.log("Failed to fetch schema");
					})
					.catch(err => console.log("Error in fetching Schema", err));
				if (status) {
					switch (this.props.auth.uType) {
						case INSTALLER:
							this.props.navigation.navigate("IDashboard");
							break;
						case SURVEYOR:
							this.props.navigation.navigate("SDashboard");
							break;
						case DUSER:
							this.props.navigation.navigate("UDashboard");
							break;
						case NGO:
							this.props.navigation.navigate("NDashboard");
							break;
						default:
							console.log("default");
							break;
					}
				} else {
					this.props.navigation.navigate("Login");
				}
			})
			.catch(err => {
				console.log(err);
			});
	}

	componentDidMount() {
		this._checkLogin();
	}

	render() {
		return <LoadingScreen />;
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
});

// const mapDispatchToProps = dispatch => ({
// 	checkLogin: () => dispatch(checkLogin()),
// 	getSchema: getSchema,
// });
const mapDispatchToProps = {
	checkLogin,
	getSchema,
};

export const CheckAuthScreen = connect(
	mapStateToProps,
	mapDispatchToProps
)(CheckAuth);
