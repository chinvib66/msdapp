import React, { Component } from "react";
import { View } from "react-native";
import QRScanScreen from "./QRScanner";

export default class TestScreen extends Component {
	render() {
		return <QRScanScreen />;
	}
}
