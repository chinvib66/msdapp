import {
	LOAD_INSTALLATIONS,
	LOAD_REPAIRS,
	UPDATE_REPAIR,
	UPDATE_INSTALLATION,
	LOAD_UCOMPLAINTS,
} from "./actions";
import { LOGOUT } from "../auth/actions";

const initialState = {
	installer: null,
	installations: null,
	repairs: null,
	complaints: null,
};

const updatedArray = (arr, elem) => {
	let newArr = arr.filter(ele => {
		return ele.id !== elem.id;
	});
	newArr.push(elem);
	return newArr;
};

export const installerReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOAD_INSTALLATIONS:
			return {
				...state,
				installations: action.payload.installations,
			};
		case LOAD_REPAIRS:
			return {
				...state,
				repairs: action.payload.repairs,
			};
		case LOAD_UCOMPLAINTS:
			return {
				...state,
				complaints: action.payload.complaints,
			};
		case UPDATE_REPAIR:
			return {
				...state,
				repairs: updatedArray(state.repairs, action.payload),
			};
		case UPDATE_INSTALLATION:
			return {
				...state,
				installations: updatedArray(
					state.installations,
					action.payload
				),
			};
		case LOGOUT:
			return initialState;
		default:
			return state;
	}
};
