import axios from "axios";
import { ToastAndroid } from "react-native";
import { tokenConfig } from "../helpers/actions";
import { apiHost } from "../../config";
// import RNFetchBlob from "rn-fetch-blob";
export const INSTALL = "Installation";
export const REPAIR = "Repair";

export const LOAD_INSTALLATIONS = "loadInstallations";
export const LOAD_REPAIRS = "loadRepairs";
export const LOAD_UCOMPLAINTS = "loadUComplaints";
export const UPDATE_TASK = "updateTask";
export const UPDATE_REPAIR = "updateRepair";
export const UPDATE_INSTALLATION = "updateInstallation";

export const loadInstallations = () => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.get(apiHost + "/installer/installations/", config)
			.then(res => {
				dispatch({
					type: LOAD_INSTALLATIONS,
					payload: { installations: res.data },
				});
				resolve(true);
			})
			.catch(err => {
				console.log("Error in fetching installations", err);
				resolve(false);
			});
	});

export const loadRepairs = () => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.get(apiHost + "/installer/repairs/", config)
			.then(res => {
				dispatch({
					type: LOAD_REPAIRS,
					payload: { repairs: res.data },
				});
				resolve(true);
			})
			.catch(err => console.log("Error in fetching repairs", err));
	});

export const updateTask = task => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		var dispatchType = null;
		var url = null;
		delete task.installer;
		switch (task.type) {
			case INSTALL:
				url = `${apiHost}/installer/installations/${task.id}/`;
				// dispatchType = UPDATE_INSTALLATION;
				dispatchType = loadInstallations;
				break;
			case REPAIR:
				url = `${apiHost}/installer/repairs/${task.id}/`;
				// dispatchType = UPDATE_REPAIR;
				dispatchType = loadRepairs;
				break;
			default:
				console.log("default task type....");
				break;
		}
		// console.log("t", t, "\ntask", task);
		axios
			.patch(url, task, config)
			.then(res => {
				// console.log("res", task);
				delete task.type;
				return true;
			})
			.then(stat => {
				// dispatch({ type: dispatchType, payload: task });
				dispatch(dispatchType());
				resolve(true);
				ToastAndroid.showWithGravity(
					"Updated",
					2000,
					ToastAndroid.BOTTOM
				);
			})
			.catch(err => console.log(err.response));
	});

const createMultipart = task => {
	let formData = new FormData();
	let t = { ...task };
	let images = [...t.images];
	delete t.images;
	Object.keys(t).forEach((k, i) => {
		formData.append(k, t[k]);
	});
	images.forEach((ele, index) => {
		let e = { ...ele, uri: `file://${ele.uri}`, name: ele.filename };
		formData.append(`file[${index}]`, e);
	});
	formData.append("fileCount", images.length);
	// console.log(formData);
	return formData;
};

export const createTask = task => (dispatch, getState) =>
	new Promise((resolve, reject) => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		var dispatchType = null;
		var payloadType = null;
		var url = null;
		let t = { ...task };
		switch (t.type) {
			case INSTALL:
				url = `${apiHost}/installer/installations/`;
				// dispatchType = UPDATE_INSTALLATION;
				dispatchType = loadInstallations;
				payloadType = "installations";
				delete t.type;
				break;
			case REPAIR:
				url = `${apiHost}/installer/repairs/`;
				// dispatchType = UPDATE_REPAIR;
				dispatchType = loadRepairs;
				payloadType = "repairs";
				delete t.type;
				break;
			default:
				console.log("default task type....");
				break;
		}
		if (t.images.length !== 0 && t.pid === undefined) {
			reject("Please Enter PID");
		} else {
			let formData = createMultipart({ ...t });

			axios
				.post(url, formData, {
					headers: {
						Accept: "application/json",
						...config.headers,
						"Content-Type":
							"multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d",
					},
				})
				.then(res => {
					// console.log(res.data);
					// dispatch({
					// 	type: dispatchType,
					// 	payload: res.data[payloadType],
					// });
					dispatch(dispatchType());
					ToastAndroid.showWithGravity(
						"Created",
						2000,
						ToastAndroid.BOTTOM
					);
					resolve("Success");
					// reject("Failed");
				})
				.catch(err => {
					console.log(err);
					reject("Failed");
				});
		}
	});

export const loadComplaints = () => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.get(apiHost + "/installer/complaints/", config)
			.then(res => {
				dispatch({
					type: LOAD_UCOMPLAINTS,
					payload: { complaints: res.data },
				});
				resolve(true);
			})
			.catch(err => {
				console.log("Error in fetching complaints", err);
				resolve(false);
			});
	});

export const createRepair = id => (dispatch, getState) =>
	new Promise((resolve, reject) => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.post(apiHost + "/installer/createRepair/", { pk: id }, config)
			.then(res => {
				console.log(res.data);
				// resolve(true);
				ToastAndroid.showWithGravity(
					"Created",
					2000,
					ToastAndroid.BOTTOM
				);
				dispatch(loadRepairs());
				resolve(true);
				// reject("Failed");
			})
			.catch(err => {
				console.log(err);
				reject("Some Error occured. Try refreshing dashboard");
			});
	});
// const data = rnfbFormData({ ...t });

// RNFetchBlob.fetch(
// 	"POST",
// 	url,
// 	{
// 		...config.headers,
// 		"Content-Type": "multipart/form-data",
// 	},
// 	data
// )
// 	.then(resp => resp.json())
// 	.then(res => {
// 		console.log(res.data);
// 		dispatch({
// 			type: dispatchType,
// 			payload: res.data[payloadType],
// 		});
// 		ToastAndroid.showWithGravity(
// 			"Created",
// 			2000,
// 			ToastAndroid.BOTTOM
// 		);
// 		// resolve("Success");
// 		reject("Not Failed");
// 	})
// 	.catch(err => {
// 		console.log(err);
// 		reject("eFailed");
// 	});

// const rnfbFormData = task => {
// 	let data = [];
// 	let t = { ...task };
// 	let files = [...t.files];
// 	delete t.files;
// 	Object.keys(t).forEach((k, i) => {
// 		data.push({ name: k, data: t[k] });
// 	});
// 	Object.keys(files).forEach((k, i) => {
// 		data.push({
// 			name: "file",
// 			filename: files[k].filename,
// 			type: files[k].type,
// 			data: RNFetchBlob.wrap(files[k].uri),
// 		});
// 	});
// 	console.log(data);
// 	return data;
// };

// export function postData(url, params, fileURLS) {
// 	let data = new FormData();
// 	if (fileURLS) {
// 		fileURLS.forEach((ele, i) => {
// 			data.append("image", {
// 				uri: ele.uri,
// 				name: ele.filenae,
// 				type: "image/jpg",
// 			});
// 		});
// 	}
// 	_.each(params, (value, key) => {
// 		if (value instanceof Date) {
// 			data.append(key, value.toISOString());
// 		} else {
// 			data.append(key, String(value));
// 		}
// 	});
// 	const config = {
// 		method: "POST",
// 		headers: {
// 			Accept: "application/json",
// 			"Content-Type":
// 				"multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d",
// 			"Content-Language": React.NativeModules.RNI18n.locale,
// 			Authorization: "Token ABCDEF123457890",
// 		},
// 		body: data,
// 	};
// 	return fetch(API_URL + url, config).then(checkStatusAndGetJSONResponse);
// }

// fetch(url, {
// 	method: "POST",
// 	headers: {
// 		Accept: "application/json",
// 		"Content-Type":
// 			"multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d",
// 		...config.headers,
// 	},
// 	body: formData,
// })
// 	.then(res => res.json())
// 	.then(res => {
// 		console.log(res);
// 		dispatch({
// 			type: dispatchType,
// 			payload: res.data[payloadType],
// 		});
// 		ToastAndroid.showWithGravity(
// 			"Created",
// 			2000,
// 			ToastAndroid.BOTTOM
// 		);
// 		resolve("Success");
// 		// reject("Failed");
// 	})
// 	.catch(err => {
// 		console.log(err);
// 		reject("eFailed");
// 	});
