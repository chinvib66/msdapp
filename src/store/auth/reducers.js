import { LOGGED_IN, LOGOUT, GET_USER } from "./actions";
import { INSTALLER, SURVEYOR } from "../index";

const initialState = {
	loggedIn: false,
	authToken: null,
	uid: null,
	uType: null,
};

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOGGED_IN:
			return {
				// loggedIn: true,
				// authToken:
				// 	"6e1fd97700addf8e5e107bddcddbc2c607fac583cb84823ebce55b5a466cf76b",
				// uType: INSTALLER,
				...action.payload,
			};
		case LOGOUT:
			return initialState;
		case GET_USER:
			return state;
		default:
			return state;
	}
};
