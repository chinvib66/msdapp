import { INSTALLER } from "..";
import axios from "axios";
import { tokenConfig } from "../helpers/actions";
import { apiHost } from "../../config";
export const LOGGED_IN = "loggedIn";
export const LOGOUT = "logout";
export const GET_USER = "getUser";

export const checkLogin = () => (dispatch, getState) =>
	new Promise((resolve, reject) => {
		state = getState().auth;
		if (state.loggedIn === true) {
			axios
				.get(apiHost + "/auth/check/", {
					headers: {
						Authorization: `Token ${state.authToken}`,
					},
				})
				.then(res => {
					if (res.data.status) resolve(true);
					else resolve(false);
				})
				.catch(err => {
					dispatch({
						type: LOGOUT,
					});
					reject(err);
				});
		} else {
			dispatch({
				type: LOGOUT,
			});
			resolve(false);
		}
	});

export const login = (uid, password) => dispatch =>
	new Promise((resolve, reject) => {
		// API Request
		axios
			.post(apiHost + "/auth/login/", {
				username: uid,
				password: password,
			})
			.then(res => {
				dispatch({
					type: LOGGED_IN,
					payload: {
						loggedIn: true,
						authToken: res.data.token, //"6e1fd97700addf8e5e107bddcddbc2c607fac583cb84823ebce55b5a466cf76b",
						uid: res.data.msduser.uid,
						uType: res.data.msduser.utype,
					},
				});
				resolve(true);
			})
			.catch(err => {
				console.log("Error occured while logging in", err);
				reject("Error occured while logging in", err);
			});
	});

export const logout = () => (dispatch, getState) =>
	new Promise(resolve => {
		// API Request
		let state = getState().auth;
		// console.log(state);
		var config = tokenConfig(state.authToken);
		axios
			.post(apiHost + "/auth/logout/", null, config)
			.then(() => {
				dispatch({
					type: LOGOUT,
				});
				resolve(true);
			})
			.catch(err => {
				console.log(err);
				resolve(err);
			});
	});

export const getUser = () => (dispatch, getState) =>
	new Promise(resolve => {
		let state = getState().auth;
		var config = tokenConfig(state.authToken);
		axios
			.get(apiHost + "/auth/user/", config)
			.then(res => {
				resolve(res.data);
			})
			.catch(err => resolve(false));
	});
