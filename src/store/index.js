import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import AsyncStorage from "@react-native-community/async-storage";
import { createLogger } from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

import { authReducer } from "./auth/reducers";
import { installerReducer } from "./installer/reducers";
import { surveyorReducer } from "./surveyor/reducers";
import { helpersReducer } from "./helpers/reducers";
import { duserReducer } from "./duser/reducers";

export const DUSER = "DU";
export const INSTALLER = "DI";
export const SURVEYOR = "DS";
export const NGO = "DN";

const storageConfig = {
	storage: AsyncStorage,
	stateReconciler: autoMergeLevel2,
};

const rootConfig = {
	key: "root",
	...storageConfig,
};

const authConfig = {
	key: "auth",
	...storageConfig,
};

const installerConfig = {
	key: "installer",
	...storageConfig,
};

const surveyorConfig = {
	key: "surveyor",
	...storageConfig,
};

const duserConfig = {
	key: "duser",
	...storageConfig,
};

const ngoConfig = {
	key: "ngo",
	...storageConfig,
};

const helpersConfig = {
	key: "helpers",
	...storageConfig,
};

const rootReducer = combineReducers({
	auth: persistReducer(authConfig, authReducer),
	installer: persistReducer(installerConfig, installerReducer),
	surveyor: persistReducer(surveyorConfig, surveyorReducer),
	duser: persistReducer(duserConfig, duserReducer),
	helpers: persistReducer(helpersConfig, helpersReducer),
});

export default function configureStore() {
	const middlewares = [thunkMiddleware];
	const middleWareEnhancer = applyMiddleware(...middlewares);

	let store = createStore(
		rootReducer,
		composeWithDevTools(middleWareEnhancer)
	);
	let persistor = persistStore(store);

	return { store, persistor };
}
