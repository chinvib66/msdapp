import { LOAD_COMPLAINTS, UPDATE_COMPLAINTS } from "./actions";
import { LOGOUT } from "../auth/actions";

const initialState = {
	duser: null,
	complaints: null,
};

const updatedArray = (arr, elem) => {
	if (arr === (undefined || null || [])) return [elem];
	else {
		let newArr = arr.filter(ele => {
			return ele.id !== elem.id;
		});
		newArr.push(elem);
		return newArr;
	}
};

export const duserReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOAD_COMPLAINTS:
			return {
				...state,
				complaints: action.payload.complaints,
			};
			break;
		case UPDATE_COMPLAINTS:
			let a = updatedArray(state.complaints, action.payload);
			console.log(state);
			return {
				...state,
				complaints: updatedArray(state.complaints, action.payload),
			};
		case LOGOUT:
			return initialState;
			break;
		default:
			return state;
			break;
	}
};
