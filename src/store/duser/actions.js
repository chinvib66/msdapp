import axios from "axios";
import { ToastAndroid } from "react-native";
import { tokenConfig } from "../helpers/actions";
import { apiHost } from "../../config";

export const COMPLAINT = "Complaint";

export const LOAD_COMPLAINTS = "loadComplaints";
export const UPDATE_COMPLAINTS = "updateComplaints";

export const loadComplaints = () => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.get(apiHost + "/duser/complaints/", config)
			.then(res => {
				dispatch({
					type: LOAD_COMPLAINTS,
					payload: { complaints: res.data },
				});
				resolve(true);
			})
			.catch(err => {
				console.log("Error in fetching complaints", err);
				resolve(false);
			});
	});

const createMultipart = task => {
	let formData = new FormData();
	let t = { ...task };
	let images = [...t.images];
	delete t.images;
	Object.keys(t).forEach((k, i) => {
		formData.append(k, t[k]);
	});
	images.forEach((ele, index) => {
		let e = { ...ele, uri: `file://${ele.uri}`, name: ele.filename };
		formData.append(`file[${index}]`, e);
	});
	formData.append("fileCount", images.length);
	// console.log(formData);
	return formData;
};

export const createTask = task => (dispatch, getState) =>
	new Promise((resolve, reject) => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		var dispatchType = null;
		var payloadType = null;
		var url = null;
		let t = { ...task };
		switch (t.type) {
			case COMPLAINT:
				url = `${apiHost}/duser/complaints/`;
				// dispatchType = UPDATE_COMPLAINTS;
				dispatchType = loadComplaints;
				payloadType = "complaints";
				delete t.type;
				break;
			// case FEEDBACK:
			// 	url = `${apiHost}duser/feedback/`;
			// 	dispatchType = UPDATE_FEEDBACKS;
			// 	payloadType = "repairs";
			// 	delete t.type;
			// 	break;
			default:
				console.log("default task type....");
				break;
		}
		console.log(t);
		if (t.images.length !== 0 && t.pid === undefined) {
			reject("Please Enter PID");
		} else {
			let formData = createMultipart({ ...t });

			axios
				.post(url, formData, {
					headers: {
						Accept: "application/json",
						...config.headers,
						"Content-Type":
							"multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d",
					},
				})
				.then(res => {
					// console.log(res.data);
					// dispatch({
					// 	type: dispatchType,
					// 	payload: res.data[payloadType],
					// });
					dispatch(dispatchType());
					ToastAndroid.showWithGravity(
						"Created",
						2000,
						ToastAndroid.BOTTOM
					);
					resolve("Success");
					// reject("Failed");
				})
				.catch(err => {
					console.log(err);
					reject("Some Error occured. Try refreshing dashboard");
				});
		}
	});
