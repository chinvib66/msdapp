import { LOAD_SURVEYS, UPDATE_SURVEY } from "./actions";
import { LOGOUT } from "../auth/actions";

const initialState = {
	surveyor: null,
	surveys: null,
};

const updatedArray = (arr, elem) => {
	let newArr = arr.filter(ele => {
		return ele.id !== elem.id;
	});
	newArr.push(elem);
	return newArr;
};

export const surveyorReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOAD_SURVEYS:
			return {
				...state,
				surveys: action.payload.surveys,
			};
		case UPDATE_SURVEY:
			return {
				...state,
				surveys: updatedArray(state.surveys, action.payload),
			};
		case LOGOUT:
			return initialState;
		default:
			return state;
	}
};
