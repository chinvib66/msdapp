import axios from "axios";
import { ToastAndroid } from "react-native";
import { tokenConfig } from "../helpers/actions";
import { apiHost } from "../../config";

export const SURVEY = "Survey";
export const LOAD_SURVEYS = "loadSurveys";
export const UPDATE_TASK = "updateTask";
export const UPDATE_SURVEY = "updateSurvey";
export const surveyorBasePath = apiHost + "surveyor/";

export const loadSurveys = () => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		axios
			.get(surveyorBasePath + "/surveys/", config)
			.then(res => {
				dispatch({
					type: LOAD_SURVEYS,
					payload: { surveys: res.data },
				});
				resolve(true);
			})
			.catch(err => {
				console.log("Error in fetching surveys", err);
				resolve(false);
			});
	});

export const updateTask = task => (dispatch, getState) =>
	new Promise(resolve => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		var dispatchType = null;
		var url = null;
		delete task.surveyor;
		switch (task.type) {
			case SURVEY:
				url = `${surveyorBasePath}/surveys/${task.id}/`;
				// dispatchType = UPDATE_SURVEY;
				dispatchType = loadSurveys;
				break;
			default:
				console.log("default task type...");
				break;
		}
		axios
			.patch(url, task, config)
			.then(res => {
				delete task.type;
				return true;
			})
			.then(stat => {
				// dispatch({ type: dispatchType, payload: task });
				dispatch(dispatchType());
				resolve(true);
				ToastAndroid.showWithGravity(
					"Updated",
					2000,
					ToastAndroid.BOTTOM
				);
			})
			.catch(err => console.log(err));
	});

export const createTask = task => (dispatch, getState) =>
	new Promise((resolve, reject) => {
		var state = getState();
		var config = tokenConfig(state.auth.authToken);
		var dispatchType = null;
		var payloadType = null;
		var url = null;
		switch (task.type) {
			case SURVEY:
				url = `${surveyorBasePath}/surveys/`;
				// dispatchType = UPDATE_SURVEY;
				dispatchType = loadSurveys;
				payloadType = "survey";
				delete task.type;
				break;
			default:
				console.log("default task type...");
				break;
		}
		axios
			.post(url, task, config)
			.then(res => {
				// console.log("resdata", res.data);
				dispatch({
					type: dispatchType,
					payload: res.data[payloadType],
				});
				dispatch(dispatchType());
				ToastAndroid.showWithGravity(
					"Created",
					2000,
					ToastAndroid.BOTTOM
				);
				resolve("Success");
			})
			.catch(err => {
				console.log(err);
				reject("Some error occured. Try refreshing dashboard");
			});
	});
