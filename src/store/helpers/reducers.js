import { GET_SCHEMA } from "./actions";
import { LOGOUT } from "../auth/actions";

const initialState = {
	status: null,
	schema: {
		msduser: null,
		installer: {
			installer: null,
			installation: null,
			repair: null,
		},
		surveyor: {
			surveyor: null,
			survey: null,
		},
		duser: {
			complaint: null,
		},
	},
};

export const helpersReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_SCHEMA:
			return {
				...state,
				schema: action.payload.schema,
			};
		case LOGOUT:
			return initialState;
		default:
			return state;
	}
};
