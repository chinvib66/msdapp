import axios from "axios";
import { apiHost } from "../../config";

export const GET_SCHEMA = "getSchema";

export const tokenConfig = token => {
	if (token !== null) return { headers: { Authorization: `Token ${token}` } };
	else return null;
};

export const getSchema = () => dispatch =>
	new Promise(resolve => {
		console.log("Fetching Schema");
		axios
			.get(apiHost + "/fullSchema/")
			.then(res => {
				dispatch({
					type: GET_SCHEMA,
					payload: res.data,
				});
				console.log("Got Schema");
				resolve(true);
			})
			.catch(err => {
				console.log("error in getting schema", err);
				resolve(false);
			});
	});
